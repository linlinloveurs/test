from __future__ import print_function
import json
import hashlib
import os
from virus_total_apis import PublicApi as VirusTotalPublicApi


API_KEY = '3c48a0c06245b9da20089b33d934bf57c2ac47691cd838aab1df945a5af16a5e'
text = os.path.abspath("sample_hash_input.txt")

with open(text,"r") as f:
    string = f.read()

resource = hashlib.sha256(string.encode('utf-8')).hexdigest()
vt = VirusTotalPublicApi(API_KEY)

response = vt.get_file_report(resource)
json_results = json.dumps(response, sort_keys=False, indent=4)
json_results_string = json_results.replace("'", "\"")
json_results_dir = json.loads(json_results_string)

scan_date = json_results_dir['results']['scan_date']
hash_value_Sha256 = json_results_dir['results']['sha256']
engines_detected = json_results_dir['results']['total']
fortinet = json.dumps(json_results_dir['results']['scans']['Fortinet'])


def printTohtml(Alist, htmlfile):    
    html =  "<html>\n<head></head>\n<style>p { margin: 0 !important; }</style>\n<body>\n"
    html += '\n<p>' + "hash_value (Sha256):" + '</p>\n'
    html += '\n<p>' + hash_value_Sha256 + '</p>\n'
    html += '\n<p>' + "Fortinet detection name:" + '</p>\n'
    html += '\n<p>' + fortinet + '</p>\n'
    html += '\n<p>' + "Number of engines detected:" + '</p>\n'
    html += '\n<p>' + str(engines_detected) + '</p>\n'
    html += '\n<p>' + "Scan Date:" + '</p>\n'
    html += '\n<p>' + str(scan_date) + '</p>\n'

    with open(htmlfile, 'w') as f:
        f.write(html + "\n</body>\n</html>")


ans =  [hash_value_Sha256, fortinet, engines_detected, scan_date]
printTohtml(ans, 'result.html')
